# React-Tetris

This is the result of a tutorial I made. 
It is originally from Thomas Weibenfalk (https://www.youtube.com/watch?v=ZGOaCxX8HIU)

To run the game, simply download the files and start with `npm start`